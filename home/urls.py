from django.urls import path,include
from home import views

from rest_framework.urlpatterns import  format_suffix_patterns

urlpatterns = [
    path('',views.index,name='index'),
    path('results',views.results,name='results'),
    path('api/v1/post',views.NSAIList.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)