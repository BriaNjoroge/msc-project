from django.http import JsonResponse
from django.shortcuts import render
from .models import NSAI


from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import NSAISerializer
from django.http import Http404



# Create your views here.
def index(request):
    return render(request, 'index.html')

def results(request):

    if request.method == 'POST':
        photo =  request.FILES["photo"]

        question = request.POST["question"]

        NSAI(photo=photo,question=question).save()

        context = {
                       "question": question
        }

        # context = serializers.serialize('json',context)
        print(context)

        return JsonResponse(context)

    print("No cont")

    return render(request, 'index.html')



class NSAIList(APIView):
    """
        Retrieve, update or delete a snippet instance.
        """



    def post(self, request):
        serializer = NSAISerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)