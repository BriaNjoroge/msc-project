function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(700)
                .height(400);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('submit', 'form', function (e) {
    var supportMsg = document.getElementById('msg');

    // var data = new FormData(this);
    var ofile=document.getElementById('id_photo').files[0];

    e.preventDefault();


    $.ajax({
        url: "/results",
        enctype: 'multipart/form-data',
        type: 'POST',
        data: {
            photo: ofile,
            question: $('#id_question').val(),
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            action: 'post'
        },

        dataType: 'json',

        success: function (data) {
            supportMsg.innerHTML =data.question;


        }

    });
    // $.ajax({
    //     url: "/results",
    //     enctype: 'multipart/form-data',
    //     type: 'POST',
    //     data: data,
    //     processData: false,
    //     contentType: false,
    //
    //     dataType: 'json',
    //
    //     success: function (data1) {
    //         supportMsg.innerHTML = data1.question;
    //
    //
    //     }
    //
    // });
});
