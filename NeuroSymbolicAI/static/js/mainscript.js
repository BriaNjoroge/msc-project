function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(700)
                .height(400);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('submit', 'form', function (e) {
    var supportMsg = document.getElementById('msg');
    // $.ajax({
    //     url: "/results",
    //
    //     type: 'POST',
    //     data: {
    //         photo: $('#id_photo').val(),
    //         question: $('#id_question').val(),
    //         csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
    //         action: 'post'
    //     },
    //
    //     dataType: 'json',
    //
    //     success: function (data) {
    //         supportMsg.innerHTML =data.question;
    //
    //
    //     }
    //
    // });

    var data = new FormData($('form').get(0));

    $.ajax({
        url: "/results",

        type: 'POST',
        data: data,
        processData: false,
        contentType: false,

        dataType: 'json',

        success: function (data) {
            supportMsg.innerHTML = data.question;


        }

    });
    e.preventDefault();
});
